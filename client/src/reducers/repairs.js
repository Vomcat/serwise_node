import {
  ADD_REPAIR,
  GET_REPAIR,
  GET_REPAIRS,
  DELETE_REPAIR,
  REPAIR_ERROR
} from "../actions/type";

const initialState = {
  repairs: [],
  repair: null,
  loading: true,
  error: {}
};

export default function(state = initialState, action) {
  const { type, payload } = action;

  switch (type) {
    case ADD_REPAIR:
      return {
        ...state,
        repair: [...state.repairs, payload],
        loading: false
      };

    case GET_REPAIR:
      return {
        ...state,
        repair: payload,
        loading: false
      };
    case GET_REPAIRS:
      return {
        ...state,
        repairs: payload,
        loading: false
      };
    case DELETE_REPAIR:
      return {
        ...state,
        repairs: state.repairs.filter(repair => repair._id !== payload),
        loading: false
      };
    case REPAIR_ERROR:
      return {
        ...state,
        error: payload,
        loading: false
      };
    default:
      return state;
  }
}
